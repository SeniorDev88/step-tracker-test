import React from 'react';
import { Pedometer } from 'expo'
import { Rating } from 'react-native-ratings'
import Modal from 'react-native-modal'
import { Table, Row, Rows } from 'react-native-table-component';
import { EventSubscription } from 'fbemitter';
import {
  Text,
  View,
  TextInput,
  TouchableHighlight,
  TouchableWithoutFeedback,
  AsyncStorage,
  Keyboard,
  ScrollView
} from 'react-native';
import { Styles } from './styles'

interface IState {
  isPedometerAvailable: string,
  showRatingModal: boolean,
  rating: number,
  comment: string,
  tableHead: string[],
  tableData: string[][],
  currentStepCount: number,
  currentTrackIndex: number
}

export default class HomeScreen extends React.Component<{}, IState> {
  _subscription?: EventSubscription
  _unsubscription?: EventSubscription
  scrollView = React.createRef<ScrollView>()

  state: IState = {
    isPedometerAvailable: 'checking',
    showRatingModal: false,
    rating: 5,
    comment: '',
    tableHead: ['Rating', 'Comment'],
    tableData: [],
    currentStepCount: 0,
    currentTrackIndex: 0
  }

  async componentDidMount() {
    this._subscribe()

    const strRatings = await AsyncStorage.getItem('ratings')
    const ratings = strRatings ? JSON.parse(strRatings) : []
    this.setState({ tableData: ratings })
  }

  componentWillUnmount() {
    this._unsubscribe()
  }

  _subscribe = () => {
    this._subscription = Pedometer.watchStepCount(async ({ steps }: { steps: number }) => {
      const { currentTrackIndex, tableData } = this.state

      this.setState({ currentStepCount: steps })

      if (Math.floor(steps / 10) > currentTrackIndex) {

        tableData.push(['', 'I walked 100 steps.'])

        this.setState({ tableData, currentTrackIndex: currentTrackIndex + 1 })

        await AsyncStorage.setItem('ratings', JSON.stringify(tableData))
      }
    });

    Pedometer.isAvailableAsync().then(
      result => {
        this.setState({
          isPedometerAvailable: String(result)
        });
      },
      error => {
        this.setState({
          isPedometerAvailable: "Could not get isPedometerAvailable: " + error
        });
      }
    );
  };

  _unsubscribe = () => {
    this._subscription && this._subscription.remove();
    this._subscription = undefined;
  };

  addRating = async () => {
    const { rating, comment, tableData } = this.state

    tableData.push([rating.toString(), comment])
    this.setState({ tableData })

    await AsyncStorage.setItem('ratings', JSON.stringify(tableData))
    this.setState({ showRatingModal: !this.state.showRatingModal })
  }

  clearAll = () => {
    this.setState({ tableData: [] })
    AsyncStorage.clear()
  }

  render() {
    const { tableHead, tableData, comment, showRatingModal, isPedometerAvailable, currentStepCount } = this.state
    return (
      <View style={Styles.container}>
        <View style={{ width: '90%' }}>
          <Table borderStyle={{ borderColor: '#C1C0B9' }}>
            <Row data={tableHead} flexArr={[1, 5]} style={Styles.head} textStyle={Styles.text} />
          </Table>
          <ScrollView
            ref={this.scrollView}
            style={Styles.dataWrapper}
            onContentSizeChange={(width, height) => this.scrollView.current && this.scrollView.current.scrollToEnd({ animated: true })}
          >
            <Table borderStyle={{ borderColor: '#C1C0B9' }}>
              <Rows data={tableData} flexArr={[1, 5]} style={Styles.row} textStyle={Styles.text} />
            </Table>
          </ScrollView>
        </View>
        <Modal
          isVisible={showRatingModal}
          backdropColor={"gray"}
          backdropOpacity={1}
          animationIn="zoomInDown"
          animationOut="zoomOutUp"
          animationInTiming={1000}
          animationOutTiming={1000}
          backdropTransitionInTiming={1000}
          backdropTransitionOutTiming={1000}
          onBackdropPress={() => { Keyboard.dismiss() }}
        >
          <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss() }}>
            <View style={Styles.modalContent}>
              <Rating
                ratingCount={10}
                imageSize={30}
                style={{ paddingVertical: 20 }}
                onFinishRating={rating => this.setState({ rating })}
              />
              <TextInput
                style={Styles.commentText}
                multiline={true}
                numberOfLines={4}
                placeholder='Input your comment'
                onChangeText={comment => this.setState({ comment })}
                value={comment}
              />
              <TouchableHighlight onPress={this.addRating}>
                <View style={Styles.button}>
                  <Text>Add</Text>
                </View>
              </TouchableHighlight>
            </View>
          </TouchableWithoutFeedback>
        </Modal>

        <View style={Styles.btnContainer}>
          <TouchableHighlight onPress={() => this.setState({ showRatingModal: true, rating: 5, comment: '' })}>
            <View style={Styles.button}>
              <Text>Input Rating</Text>
            </View>
          </TouchableHighlight>

          <TouchableHighlight onPress={this.clearAll}>
            <View style={Styles.button}>
              <Text>Clear All</Text>
            </View>
          </TouchableHighlight>
        </View>

        <Text>
          Is Available Step Counter?: {isPedometerAvailable}
        </Text>
        <Text>Walk! And watch this go up: {currentStepCount}</Text>
      </View>
    );
  }
}
